import { AdvancedColumnFormatter } from '@universis/ngx-tables';
import {DatePipe} from '@angular/common';
import {ConfigurationService} from '@universis/common';
export class DateFormatter extends AdvancedColumnFormatter {
    render(data, type, row, meta) {
      // get column
      let day: any;
      let date: any;
      let dot: any;
      let time: any;
      const column = meta.settings.aoColumns[meta.col];
      if (column && column.data) {
        const currLocale = this.injector.get(ConfigurationService).currentLocale;
        const datePipe: DatePipe = new DatePipe(currLocale);
        // const datePipe: DatePipe = new DatePipe('en');// Better use currentlanguage but for now el is unavailable
        day = datePipe.transform(data, 'd');
        if (day.length === 1){
          day += '&nbsp;&nbsp;';
        }
        date =  datePipe.transform(data, 'MMM, EEE'); // + datePipe.transform(row.endDate, 'd MMM, EEE').toUpperCase();
        dot = DateFormatter.stringToColour(row.name);
        time = datePipe.transform(data, 'HH:mm') + ' - ' + datePipe.transform(row.endDate, 'HH:mm');
        return `<span style="font-size: 140%; color: #3c4043;"> ${day}</span>
        <span lang="${currLocale}" class="text-uppercase" style="font-size: 90%; padding-left: 3px; color: #70757a;">${date}</span>
        <span class="px-1" style="height: 13px; width: 13px; background-color: ${dot}; border-radius: 50%; display: inline-block;"></span>
        <span class="font-lg">${time}</span>`;
      }
      return data;
    }

    static stringToColour(str) {
      let i;
      if (str == null) {
        str = 'placeholder';
      }
      let hash = 0;
      for (i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
      }
      let colour = '#';
      for (i = 0; i < 3; i++) {
        const value = (hash >> (i * 8)) & 0xFF;
        colour += ('00' + value.toString(16)).substr(-2);
      }
      return colour;
    }

    constructor( private configurationService: ConfigurationService) {
      super();
    }
  }

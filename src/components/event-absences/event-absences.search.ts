export const EVENT_ABSENCES_SEARCH_CONFIGURATION = {
  "model": "",
  "components": [
    {
      "label": "Columns",
      "columns": [
        {
          "components": [
            {
              "label": "Events.Student.FamilyName",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "studentFamilyName",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Events.Student.GivenName",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "studentGivenName",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
      ],
      "tableView": false,
      "key": "department",
      "type": "columns",
      "input": false,
      "path": "columns"
    },
    {
      "label": "Columns",
      "columns": [
        {
          "components": [
            {
              "label": "Events.Student.Identifier",
              "labelPosition": "top",
              "spellcheck": true,
              "widget": {
                "type": "input"
              },
              "key": "studentIdentifier",
              "type": "textfield",
              "input": true
            }
          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        }
      ],
      "tableView": false,
      "key": "columns1",
      "type": "columns",
      "input": false,
      "path": "columns1"
    }
  ]
}

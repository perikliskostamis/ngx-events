import { TrueFalseFormatter } from '@universis/ngx-tables';

export const EVENTS_CONFIGURATION = {
  'title': 'Events',
  'settings': {
    'i18n': {
      'el': {}
    }
  },
  'model': 'instructors/me/teachingEvents',
  'searchExpression': '(indexof(name, \'${text}\') ge 0) or (day(startDate) eq ${/^\\d+$/.test(text) ? text : 0}) or (indexof(description, \'${text}\') ge 0)',
  'selectable': false,
  'smartSelect': false,
  'defaults': {
    'orderBy': 'startDate desc',
    'filter': '((superEvent ne null) or (eventHoursSpecification eq null))',
    'expand': 'sections'
  },
  'multipleSelect': true,
  'columns': [

    {
      'name': 'startDate',
      'property': 'startDate',
      'formatter': 'DateFormatter',
      'label': 'Date',
      'title': 'Date'
    },
    {
      'name': 'endDate',
      'property': 'endDate',
      'hidden': true
    },
    {
      'name': 'courseClass/title',
      'property': 'courseClass',
      'title': 'Class',
      'label': 'Class',
      'hidden': true
    },
    {
      'name': 'name',
      'property': 'name',
      'title': 'Name',
      'label': 'Name',
      'hidden': false
    },
    {
      'name': 'description',
      'property': 'description',
      'title': 'Description',
      'label': 'Description',
      'hidden': false
    },
    {
      'name': 'sections',
      'property': 'sections',
      'title': 'Sections',
      'label': 'Sections',
      'hidden': false,
      'sortable': false,
      'virtual': true,
      'formatters': [
          {
            'formatter': 'TemplateFormatter',
            'formatString': "${sections && Array.isArray(sections) && sections.length > 0 ?'<span class=\"text-center\">' + sections.map(x=> x.name).join(',') + '</span>':'-'}"
          }
      ]
    },
    {
      'name': 'performer/alternateName',
      'property': 'performer',
      'title': 'Performer',
      'label': 'Performer'
    },
    {
      'name': 'eventStatus/name',
      'property': 'eventStatus',
      'formatter': 'StatusFormatter',
      'title': 'Status',
      'label': 'Status',
      'hidden': false
    },
    {
      'name': 'id',
      'formatter': 'ButtonFormatter',
      'className': 'text-center',
      'sortable': false,
      'formatOptions': {
        'buttonContent': '<i class="far fa-eye text-indigo"></i>',
        'buttonClass': 'btn btn-default',
        'commands': [
          '${id}',
          'attendance'
        ]
      }
    },
    {
      'name': 'id',
      'property': 'eventId',
      'sortable': false,
      'formatter': 'ButtonFormatter',
      'formatString': '(modal:delete)',
      'className': 'text-center',
      'formatOptions': {
        'buttonContent': '<i class="fa fa-trash text-indigo" aria-hidden="true"></i>',
        'buttonClass': 'btn btn-default',
        'commands': [
          '${id}', 'delete'
        ],
        'navigationExtras': {
          'replaceUrl': false,
          'skipLocationChange': true
        }
      }
    }
  ],
  'searches': [  ],
  'paths' : [  ],
  "criteria": [
    {
      "name": "eventName",
      "filter": "(indexof(name, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "sections",
      "filter": "(sections/section eq ${value})",
      "type": "text"
    },
    {
      "name": "minDate",
      "filter": "(startDate ge '${new Date(value).toISOString()}')",
      "type": "text"
    },
    {
      "name": "maxDate",
      "filter": "(startDate le '${new Date(value).toISOString()}')",
      "type": "text"
    },
    {
      "name": "status",
      "filter": "(eventStatus eq '${value}')",
      "type": "text"
    }
  ]
};
